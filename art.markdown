---
layout: page
title: Design  
permalink: /art-design/
---

I love architecture and design. I make some illustrations for professional projects or just for fun. Here are some work in progress illustrations.
Still learning some techniques and copy paste from available illustrations.

#### Islamic patterns

Work in progress [figma](https://www.figma.com/file/GH4v7izBPOi5J0ShFDbb50/islamic-patterns?node-id=0%3A1)

![islamic pattern](/assets/images/design/islamic_patterns/pattern_1.png)
{: class="u-text-center"}

![islamic pattern](/assets/images/design/islamic_patterns/pattern_2.png)
{: class="u-text-center"}


#### Random Patterns 
Work in progress [figma](https://www.figma.com/file/acOk2BXKc6vDRbFBd2CcRs/random-patterns?node-id=0%3A1)

![random patterns](/assets/images/figma/patterns.png)
{: class="u-text-center"}


#### Figma Lines assets
 
Link to [figma](https://www.figma.com/file/ZJLcWD8c1stEl1vkKFIzSR/Lines?node-id=0%3A1)

Assets used for [lines](http://lines.380squares.xyz) 

![charts](/assets/images/design/lines/charts.png)
{: class="u-text-center max-500"}


![report placeholder](/assets/images/design/lines/report-1.png)
{: class="u-text-center max-500"}

![report placeholder](/assets/images/design/lines/report-2.png)
{: class="u-text-center max-500"}


#### Random 
 
Randoms drawing using sumsung notes  

![charts](/assets/images/design/notes/Notes_191229_142112_1.jpg)
{: class="u-text-center max-500"}

![report placeholder](/assets/images/design/notes/Notes_200103_113143.jpg)
{: class="u-text-center max-500"}

![report placeholder](/assets/images/design/notes/Notes_201106_083346_1.jpg)
{: class="u-text-center max-500"}

![report placeholder](/assets/images/design/notes/Notes_201106_083613.jpg)
{: class="u-text-center max-500"}

![report placeholder](/assets/images/design/notes/Re_201101_143849.jpg)
{: class="u-text-center max-500"}

#### Generative Art
I try to play with code to generate some art but i still novice, from time to time i try to pick an idea and use code (python or js) to show what it gives.
I will update may blog with some future work.  

  