---
layout: page
title: About
permalink: /about/
---
<br/>
<br/>
Welcome to my personal website. I'm a software engineer and
I'm running [380 squares](https://www.380squares.xyz/), a company behind products below.


<br/>


__Products__

- [__Parachute__](http://lines.380squares.xyz) (since June 2021) :
Announcements, feedbacks and analytics made easy. 

<br/>

__Previous (discontinued)__

<br/>
I built, launched and dropped out many apps since 2014. You may like to take a look.


- Unk analytics (2019) Simple anonymous analytics for web apps :
[Code source ](https://gitlab.com/unknown-inc/web-app)

- Keedo (2018) Sass and on promise school management solutions :
[Code source ](https://gitlab.com/keedo-)

- Bloosa (2017-2018) : A medical appointment solution.
  [Code source](https://gitlab.com/bloosa/backend)

- Yalla (2018) :
City explorer 
[Code source](https://github.com/yalla-entreprise)

- Nenvi (2015-2017) :   anti-theft solutions for farmers
[Code source](https://github.com/Primos-tn)

- MeeM (2013) :
[Code source](https://github.com/houssemFat/MeeM-Dev)

- Bloodon (2013) :
[Code source](https://github.com/houssemFat/bloodOn)

