---
layout: page
title: Failure 
permalink: /failure-cv/
background: /assets/images/20200831_183619.jpg
---
<br/>

Failure is just a one way of seeing things. 

#### Startups 
* 2018 -  Failed to relaunch meem 
* 2016/2017 -  Failed to launch Electoris by Nenvi  (an anti-theft for animals)
* 2016 -  Failed to launch Yezzi (a weekly fruit and vegetable box delivered to your door)
* 2014 -  Failed to launch meem (online education system for primary school in tunisia)
<br/>