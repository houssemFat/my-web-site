---
layout: page 
title: Books 
permalink: /books/
---
Below some of my favourite list of books that i enjoy(ed) reading and taking notes from.

##### Software / Management / startups / Business ...

[The Mythical Man-Month: Essays on Software Engineering](https://www.goodreads.com/book/show/13629.The_Mythical_Man_Month)

[The Luxury Alchemist](https://www.goodreads.com/book/show/17739496-the-luxury-alchemist)

[What is management](https://www.goodreads.com/book/show/13536357-from-the-ruins-of-empire)

[Good to Great](https://www.goodreads.com/book/show/76865.Good_to_Great)

[First break all the rules](https://www.goodreads.com/book/show/50937.First_Break_All_the_Rules)

[High Output management](https://www.goodreads.com/book/show/324750.High_Output_Management)

[Bad Blood: Secrets and Lies in a Silicon Valley Startup](https://www.goodreads.com/book/show/37976541-bad-blood)

<br/>
<br/>

[Code: The Hidden Language of Computer Hardware and Software](https://www.goodreads.com/book/show/44882.Code)

[The Art of Electronics](https://www.goodreads.com/book/show/569775.The_Art_of_Electronics)

[Programming Erlang](https://www.goodreads.com/book/show/17800216-programming-erlang)

[High Performance Browser Networking](https://www.goodreads.com/book/show/17985198-high-performance-browser-networking)

<br/>
<br/>

##### Other ....

[The African Trilogy](https://www.goodreads.com/book/show/17113.The_African_Trilogy)

[blink](https://www.goodreads.com/book/show/40102.Blink)

[(arabic )الخسوف](https://www.goodreads.com/series/85296)

[Upheaval: Turning Points for Nations in Crisis](https://www.goodreads.com/en/book/show/41716904-upheaval)

[The ruins of an Empire](https://www.goodreads.com/book/show/13536357-from-the-ruins-of-empire)

[Bird by bird](https://www.goodreads.com/book/show/12543.Bird_by_Bird)

[Outliers: The Story of Success](https://www.goodreads.com/book/show/3228917-outliers)

[Migrations](https://www.goodreads.com/book/show/42121525-migrations)