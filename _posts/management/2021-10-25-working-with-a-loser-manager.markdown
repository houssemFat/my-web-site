---
layout: post
title:  Working with a loser manager
date:   2021-10-25 21:15:32 +0100
categories: management
---


I can’t really quantify all the things that I have learned from all the companies i've worked with in my career, but bad
things happen.

This post is based on my personal experience working with bad manager(s) in IT companies.

<br/>

__TL;DR__

__Please don’t work with/for losers, they will waste your time, your energy and damage your mental and physical health.
If your company promoted or recruited a loser to work with, you are in the wrong place. A loser manager is one of the
most harmful species on earth. They are bad for business and harmful for health.__

<br/>
**_How to spot a "loser" manager?_**
<br/>

- He is an imposter, A really very bad imposter.
- He is a fake comedian.
- He can't ship any product whatever the resources he has.
- He has zero interest in your career.
- His greatest achievement is being the best friend of his boss. You will see him always with him.
- He has zero knowledge about you (and your team) . He did not know anything about team members, goals, projections,
  personal interests ...
- He prefers formal communications instead of one-to-one or personal/direct discussion. He thinks it will serve him in
  the future.
- He keeps track of everything because that’s the proof that he is doing his job.
- He is a genius and everyone should learn from him. He is the smartest one in the room. His idea is the only one to be
  executed. He “knows” everything.
- He is always right. Losers can’t make mistakes.
- He uses trashy buzzwords (technobabble) to look smart.
- He thinks management is a monthly one-to-one meeting and 1 career evaluation each year.
- For him, everything is easy to do no matter the complexity.
- For him, the client is always the king. No matter what can happen with you.
- He uses the tools he loves, not what fits better for the team. Why use a well known tool that everyone is familiar
  with, instead we should go with the tool he uses.
- He avoids conflict and direct
  confrontation ([The Five Dysfunctions of a Team](https://www.amazon.com/Five-Dysfunctions-Team-Leadership-Fable/dp/0787960756 ))
- Whatever the team did, the big credits will go to him.
- If something went wrong, “who did this ?” is his first question.
- After some months working with him, more than half of your team is already gone.
- Losers love losers, they use the same way of management and some jargon.

<br/>
<br/>
**_When/where you need a loser ?_** :
- You want your business to go bankrupt.
- You need to stop making progress.
- To create a toxic and insane place for work.
- You need the team to resign without effort.
- To make other losers.
- To lose clients/projects.

<br/>
__In the last few months, I was working with losers. I would like to thank him for teaching me how a loser should be.__
