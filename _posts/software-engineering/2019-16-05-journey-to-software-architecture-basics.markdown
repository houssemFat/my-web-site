---
layout: post
title:  On the journey to software architect - Return to basics [draft]
date:  2020-05-16 00:45:12 +0200
categories: software-engineering
---

The three big concerns of architecture: function, separation of components, and data management

**Structured programming** imposes discipline on _**direct transfer of control**_.

**Object-oriented** programming imposes discipline on __*indirect transfer of control*__.

**Functional programming** imposes _discipline_ **upon assignment**.

#books 


No [silver bullet ](https://web.archive.org/web/20160910002130/http://worrydream.com/refs/Brooks-NoSilverBullet.pdf)


* Software is not hardware
* Software is a human activity, just like art.
* You can not expect a rising 
* Essential 

<cite>
    Following  Aristotle,  I  divide  them  into  essence−the  difficulties  inherent  in  the  nature  of  the  software−and  accidents−those  difficulties  that  today  attend  its production but that are not inherent.
</cite>