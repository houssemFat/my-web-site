---
layout: post
title:  On the journey to software architect - Scalability [draft]
date:   2019-04-30 10:04:56 +0200
categories: software-engineering
---

> Mean 
	> Sum of all requests response time divided by number of requests 
	Used when you need to know response time 
> Median  (P50)
	> Percentile 50, used to know how long a user have to wait before request response 
