---
layout: post
title:  Sotftware prodcut documentation.
date:  2022-02-07 12:10:45 +0200
categories: software-engineering
---
# Documentation matters more than you think.
No matter how your product is a revolutionary, if you miss documentation it will be useless before it goes online.

### Requirements 

* SEO - User will search google with `Update sso on your_product` and expect to have the link to your documentation on first results.
* Search - User will use search bar to look for something within your product.
* Table of contents - User will browse and select a specific section of doc.
* Organisation - 
* Getting started - User will start by a simple installation or minimal setup
* Snippets

### Know your target

Based on your target, we should speak the same language end user. Let's say your product is a B2C sass application for managing events.  

### Customer (enduser) manual documentation

Based on your target, we should speak the same language end user. Let's say your product is a B2C sass application for managing events.  
* Dashboard section
* Billing section 
* Accounting section 
This is for your end user.
* Cover supports and product usage 
* Manual of a given solution or product. 

#### Api (developer) documentation

* OpenApi / GraphQL schema
* 
Check [stripe](https://stripe.com/docs/)

##### Internal api 

##### public api

* Sandbox environment
* online test environment
* Different languages snippets

#### Issues with software  

##### Setup a documentation strategy


1- Public documentation & internal documentation 

The lack of documentation in software is harmful for the whole project lifetime. With time, code changes, new integrations and features are shipped, old frameworks are deprecated, architecture changes  ....

* A good documentation strategy starts with a simple guideline.
* 
* Basic requirements 
  * Separate folders by customer target
  * Follow a clean template
  * Centralize all documentations modules (installation, requirement, api, )
  * Live with the code
  * Minimalist design. Clean titles, paragraphs, examples, href.
  * Copy from others (stripe, notion, google .....) 
  * 
* Synchronised documentation is the efficient way to keep your software up to date.


#### Poor documentation is worse than no documentation 
If ther's some kind 

Think stripe. I mean ....
#### Documentation for developer
##### Product
##### Code
##### Integration 
##### Infrastructure
##### Deployment
#### In nutshell
#### API
### Open api 
* Organise documentation
* 
* 
* Share and Review with team member
* Track history 
* Use minimalist design
* 
* use rfc 
