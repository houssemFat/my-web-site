---
layout: post
title:  How to fail quickly in you sass product
date:  2022-06-01 20:08:17 +0200
categories: software-engineering
---

In this post a address the general purpose sass application. This is a reflexion based on personal experience on some work i did.

# How to fail quickly and lose your time to market 
In the previous experiences, I have been working and launched many small to medium projects.
Errors were inevitable because sometimes we miss some critical little things.
Unless you are playing with a new framework, testing something, exploring an other tech field, once you put your product online, it's no longer yours. 
Your product is meant to solve an issue for your client. If the issue doesn't exist, your product has no sense.



In this post i will list some errors (enough to kill your product) mistakes 

#### Lose your time on design (UI/UX)
#### Start documentation after the launch
Documentation is critical and your product will never exist without it. So, pushing documentation till going online is a horror mistake.
Use the documentation as a guide for you, for your product team and the technical team before publish it. So instead of pushing that to release days, start by documentation in synchronisation with the R&D and development phases and market research.
This make your product looks real in it's development phase. It can help you identify issues and tweak it on the fly.
If anyone in your team can't work with a documentation, this is a good sign that the end user will never get it.

Documentation, support, knowledge center or manual (whatever you call for a manual of usage) is part of your product, not a "nice to have" feature.

<br/>
#### Work on your world without market research

We are not intelligent than others, we have the "same" issues and problems to fix. We will never be the first person who find that revolutionary idea. So do you research about your idea and get insights, feedbacks from the market you target.
There are always big and small players. Red and blue oceans, public and niche markets.

<br/>
#### Try to build the swiss-knife tool

Thinking of being the __one to go__ from day one kills your idea. I remember of __meem__, which was an online platform for primary school in tunisia, we were trying to be "The platform" online to do everything. From home work to courses, students management, libraries and so on
We failed, because implementing all theses features before launching with small team is a startup suicide. 

You need to do on thing but do it right, __better and for reasonable price__ than your competitors.
<br/>
<br/>
#### Adapt the newest trendy technologies 

Pick the framework/tool that create your business value and your team master. There is no [silver bullet](https://www.cgl.ucsf.edu/Outreach/pc204/NoSilverBullet.html).
Every framework has its pros and cons. In 99% of cases, you will never hit the million users in your first launch month. 
Knowing that did not mean that you can use nodejs or ruby for analysing video streaming in realtime for an inspection platform. 
In most business apps, the main bottleneck is business requirements, flows, dependencies and bad implementations not technologies.

- Start with "good enough" technology that creates your business value
- Don't follow new tech waves
- Architect the solution to be flexible for new features
- Follow one guideline and put it with the code source 
<br/>
<br/>

#### Do what's not your business core
I always remember doing sidecar features in my previous solutions before even finish the main core business code. 
Example, setup streaming log, audit log, SSO integrations, set up nomad/consul or K8s, downtime notifications, disaster recovery, looking for mqtt v5 implementations , SLA ....
All previous features are part of your business value, and you can't have a sure enough solution without resolving/setup all of them. The idea here is to adapt a 80/20 strategy by asking the right question and executing the decision on business priority order. 

Below some requirements functionalities that i delegate to other tools 

- Emails, unless you deal with emailing business
- Multi tenancy, SSO, RBACs systems (I will need them later unless they are part of my business value)
- Horizontal, vertical scaling from 0 days 
#### Making big changes take effort

A product that works is better than buggy one.

