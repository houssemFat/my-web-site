require 'json'
require 'csv'
file = File.read('hits.json')
results= JSON.parse(file)

names = []
countries = []
tags = []

CSV.open("companies.csv", "w") do |csv|
  companies =  results['results'][0]['hits']
  csv << ["name", "country", "tags", "industries"]
  companies.each do |company|
    tags.concat(company['tags'])
    names.append(company['name'])
    countries.append(company['all_locations'])
    p company['industries'].flatten
    csv << [company['name'], company['all_locations'], company['industries'].join(", "), company['tags'].join(", ")]
  end
end


def generate_tags
  tags_keys = Hash.new(0)
  tags.each do |tag|
    tags_keys[tag] += 1
  end

  CSV.open("tags.csv", "w") do |csv|
    csv << ["tag", "count"]
    tags_keys.sort_by {|_, value| value}.reverse.to_h.each do |tag, count|
      csv << [tag, count]
    end
  end
end

# count_letters_names
  p names
  names_letters = Hash.new(0)
  names.each do |name|
    name.chars.each do |l|
      names_letters[l.upcase] += 1
    end
  end
  p names_letters.sort_by {|_, value| value}.reverse.to_h
# end count_letters_names


puts countries