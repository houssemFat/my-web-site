---
layout: post
title:  Creating a plugin for figma
date:   2020-10-17 22:35:47 +0100 
categories: figma lines
---



I was recently working with figma for my [little product](http://lines.380squares.xyz/) and I discovered how amazing the tool is. Figma is a simple yet powerful tool to design and make prototypes. I already worked with inkscape which is a nice tool too. There are a lot of features and plugins that come with inkscape but you need to install the app. You should invest a significant time to get familiar with. Another drawback of inkscape is the lack of ready to use templates. It’s an open source tool and that needs more money/marketing…) .

Figma is a web based app and a desktop studio. You type the link in the browser and you have the UI within seconds. It supports file sharing and view in real time. But what I appreciated in figma is it's easy to go interface. The minimalist ui interface is so clean and you see only what you need for a given shape. In figma, you can find a lot of useful templates and plugins to help you get started. Also, Figma comes with plugin development support. This is a killer feature that lets you extend your figma app to perform unsupported tasks .

At [lines](http://lines.380squares.xyz/) we provide static and dynamic charts as a service. For now, you can create choropleth maps. You can select a shape (country in this context) and customize the background for example. You can customize the map background, countries, areas and legend. For a given shape you can set a pattern as background. So, we provide a collection of patterns that we created using figma. And while doing this task, we see the need for a figma plugin that make our job easier.

#### How figma plugin works

A figma plugin is a standard javascript application that can be run on a browser (electron and all browser support platforms...) . It can be run in different ways, standard, silent or via web worker, but for now let’s talk about the standard way.

**Main structure of a figma package**

A figma plugin contains the following three files :

* Ui.html, the ‘dialog’ or the plugin user interface that will be displayed on an iframe.

* Ui.js, the javascript code to be run inside the iframe of the plugin.

* code.js, this is the library code or the plugin execution code. This will be executed with the main figma application after receiving a message from the iframe.

**Running steps :**

1 - Ui : This is the interface where the user can define options and settings for the plugin library. This step is executed in an iframe (ui.js + ui.html)

2 - Library : This is the code that figma application will run ( inside code.js)

To send data from the ui to the code we use the [postMessage()](https://developer.mozilla.org/fr/docs/Web/API/Window/postMessage) api.

To send data from the ui to the code we use the postMessage() [https://developer.mozilla.org/fr/docs/Web/API/Window/postMessage] api .

#### The patterns plugin

The plugin can add a customized pattern background to your shape. For the first version, only colors can be customized.

I want to share how I wrote the plugin by highlighting the main steps. You can explore the full [code source](https://github.com/houssemFat/figma-patterns).

There are some main concepts that I would like to share and explain how things work behind it.

##### Step 1 - Creating and transform the patterns

First I started creating some patterns using figma. You can learn more about [how to create patterns on figma and find inspirations](https://www.figma.com/blog/how-a-designer-makes-background-patterns-to-push-her-creativity/). Below some simple patterns used for this plugin. It started by simple shapes and then some copy, paste operations and attributes modifications (scales, orientation, distances, colors ….).

![simple pattern](/assets/images/figma/patterns.png)
{: class="u-text-center max-500"}

Once we have our patterns, we export them as svgs.

Svg format is better to manipulate and handle format and colors. First we transform all the exported svg files into an array of strings so we can import them in the js code and play with colors (using the script of tools/transform.js).

Then, we optimize each svg file with [svgo](https://www.npmjs.com/package/svgo). And finally we extract all colors from stroke and fill properties by replacing them with css variables `var(--...)`.

The use of [custom variables](https://developer.mozilla.org/en-US/docs/Web/CSS/--*) allows to change values of stroke and fill.

##### 2 - UI and code

The main idea behind this plugin is to create an image tile using [ImagePaint object](https://www.figma.com/plugin-docs/api/Paint/) where the scaleMode is `TILE`. To do that, we need first to create an image. Figma lets you create images in a uint8array format, thus we should decode our image into this format. The we send it back to the plugin code.

To do this, we put an image with an svg as background into a canvas, then we get the blob and transform it to a [uint8array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Uint8Array) using the FileReader. More on how to work with images in figma (https://www.figma.com/plugin-docs/working-with-images/)

**_Tip on setting an svg as background for an image_**

As mentioned [here](https://css-tricks.com/probably-dont-base64-svg/) by [Chris Coyier](https://css-tricks.com/author/chriscoyier/) we can set a background image using a plain svg using this format `"data:image/svg+xml;utf8,"` + `escape(svgAsString)` (svgAsString is the svg outerHTML) .

Note: i used escape to handle characters because some svg files may broke the data uri.

##### Step 3 - Bundling with webpack

For simple plugins you can write js/html/css with no need for a bundling tool. But once your code becomes complex, it is necessary to split. In this case, it is important to use a bundler to transform your source code into a figma plugin structure. There is a (tutorial on figma)[https://github.com/figma/plugin-samples/blob/master/webpack] that describes how to use webpack . I copied the config from the repository but it didn’t work so instead I used another inline-webpack tool that I copied from react-create-app.

Note : I tried to make it with `parcel` but it needs a lot of plugins and configuration to work.


I tried to use the plugin to customize the map tunisia and below the result : 

![simple pattern](/assets/images/figma/tunisia.png)
{: class="u-text-center max-500"}

