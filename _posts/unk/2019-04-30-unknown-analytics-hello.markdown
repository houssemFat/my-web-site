---
layout: post
title:  "Unknown analytics"
date:   2019-04-30 10:04:56 +0200
categories: unknown-analytics api
---

We are excited to launch our new adventure in the AaaS, an advanced compeletly anonymous analytics plateform for web
sites.

Our new baby, which we consider "yet another " , there are already some nice players in this field simpleAnalytics,
usePhtomo which are fully open source.

Why [**] ,


-- Features :

--- Fully anonymous page, events --tracking-- .


We absolutely DO NOT report :

* Cookies
* User ID
* Session ID
* User location
* User ID

We do not store any data in user borwerser/devices :

* No localstorage
* No Session storage
*

How Geo-ip revere is working


- We use ip to know the request city and no other information of the cite. Once we get the
Once we get the city, the ip value will be no longer . Enabling ip <--> city is optional and it's disabled by
default. It's up to user (domain owner to decide whether to activate it or not )

- Heat map tracking : We are glad to provide such a functionality which illustrate how users navigate in pages and
react to page elements. The heat map tracking service is


- We love open source, the application is build with awesome libraries


Road map [2019 4th quarter]:


-- Servers :

Our application is hosted


-- Exemple of event structure


## Aesthetic Usability Effect

> Users often perceive aesthetically pleasing design as design that’s more usable.

## Doherty Threshold

> Productivity soars when a computer and its users interact at a pace (<400ms) that ensures that neither has to wait on the other.

## Fitts’s Law

>The time to acquire a target is a function of the distance to and size of the target.


## Hick’s Law

> The time it takes to make a decision increases with the number and complexity of choices.

## Zeigarnik Effect

> People remember uncompleted or interrupted tasks better than completed tasks.




<div class="page-container">
<div class="stackedit__html"><h3 id="get-started">Get started</h3>
	<h3 id="api-rate-limit">Api rate limit</h3>
	<p>In order to prevent a DOS attack we use Nginx page limit.</p>
	<h3 id="page-views-rate-limit-for">Page views rate limit for</h3>
	<p>Our implementation of the page views limit (monthly number of allowed views) for both essential and advanced
		plans use token bucket algorithm [<a href="https://en.wikipedia.org/wiki/Token_bucket">https://en.wikipedia.org/wiki/Token_bucket</a>].
		For each subscription plan, a maximum number of tokens (view count) for a user (for all domains) are set in
		redis then with api view call we decrement the tokens .</p>
	<div class="mermaid">
		<svg xmlns="http://www.w3.org/2000/svg" id="mermaid-svg-eLLi0ThJdgslo9bz" height="100%" width="100%"
			 style="max-width:485px;" viewBox="-50 -10 485 416">
			<g></g>
			<g>
				<line id="actor4" x1="75" y1="5" x2="75" y2="405" class="actor-line" stroke-width="0.5px"
					  stroke="#999"></line>
				<rect x="0" y="0" fill="#eaeaea" stroke="#666" width="150" height="65" rx="3" ry="3"
					  class="actor"></rect>
				<text x="75" y="32.5" style="text-anchor: middle;" dominant-baseline="central"
					  alignment-baseline="central" class="actor">
					<tspan x="75" dy="0">Your site</tspan>
				</text>
			</g>
			<g>
				<line id="actor5" x1="275" y1="5" x2="275" y2="405" class="actor-line" stroke-width="0.5px"
					  stroke="#999"></line>
				<rect x="200" y="0" fill="#eaeaea" stroke="#666" width="150" height="65" rx="3" ry="3"
					  class="actor"></rect>
				<text x="275" y="32.5" style="text-anchor: middle;" dominant-baseline="central"
					  alignment-baseline="central" class="actor">
					<tspan x="275" dy="0">Our server</tspan>
				</text>
			</g>
			<defs>
				<marker id="arrowhead" refX="5" refY="2" markerWidth="6" markerHeight="4" orient="auto">
					<path d="M 0,0 V 4 L6,2 Z"></path>
				</marker>
			</defs>
			<defs>
				<marker id="crosshead" markerWidth="15" markerHeight="8" orient="auto" refX="16" refY="4">
					<path fill="black" stroke="#000000" style="stroke-dasharray: 0px, 0px;" stroke-width="1px"
						  d="M 9,2 V 6 L16,4 Z"></path>
					<path fill="none" stroke="#000000" style="stroke-dasharray: 0px, 0px;" stroke-width="1px"
						  d="M 0,1 L 6,7 M 6,1 L 0,7"></path>
				</marker>
			</defs>
			<g>
				<text x="175" y="93" style="text-anchor: middle;" class="messageText">Ping</text>
				<line x1="75" y1="100" x2="275" y2="100" class="messageLine0" stroke-width="2" stroke="black"
					  style="fill: none;" marker-end="url(#arrowhead)"></line>
			</g>
			<g>
				<text x="275" y="153" style="text-anchor: middle;" class="messageText">Fight against hypochondria
				</text>
				<path d="M 275,160 C 335,150 335,190 275,180" class="messageLine0" stroke-width="2" stroke="black"
					  style="fill: none;"></path>
			</g>
			<g>
				<text x="275" y="238" style="text-anchor: middle;" class="messageText">Fight against hypochondria
				</text>
				<path d="M 275,245 C 335,235 335,275 275,265" class="messageLine0" stroke-width="2" stroke="black"
					  style="fill: none;"></path>
			</g>
			<g>
				<line x1="165" y1="110" x2="385" y2="110" class="loopLine"></line>
				<line x1="385" y1="110" x2="385" y2="285" class="loopLine"></line>
				<line x1="165" y1="285" x2="385" y2="285" class="loopLine"></line>
				<line x1="165" y1="110" x2="165" y2="285" class="loopLine"></line>
				<line x1="165" y1="200" x2="385" y2="200" class="loopLine"
					  style="stroke-dasharray: 3px, 3px;"></line>
				<polygon points="165,110 215,110 215,123 206.6,130 165,130" class="labelBox"></polygon>
				<text x="172.5" y="125" fill="black" class="labelText">
					<tspan x="172.5" fill="black">alt</tspan>
				</text>
				<text x="275" y="125" style="text-anchor: middle;" fill="black" class="loopText">
					<tspan x="275" fill="black">[ Healthcheck ]</tspan>
				</text>
				<text x="275" y="215" style="text-anchor: middle;" fill="black" class="loopText">
					<tspan x="275" fill="black">[ else ]</tspan>
				</text>
			</g>
			<g>
				<text x="175" y="313" style="text-anchor: middle;" class="messageText">How about you John?</text>
				<line x1="275" y1="320" x2="75" y2="320" class="messageLine0" stroke-width="2" stroke="black"
					  style="fill: none;" marker-end="url(#arrowhead)"></line>
			</g>
			<g>
				<rect x="0" y="340" fill="#eaeaea" stroke="#666" width="150" height="65" rx="3" ry="3"
					  class="actor"></rect>
				<text x="75" y="372.5" style="text-anchor: middle;" dominant-baseline="central"
					  alignment-baseline="central" class="actor">
					<tspan x="75" dy="0">Your site</tspan>
				</text>
			</g>
			<g>
				<rect x="200" y="340" fill="#eaeaea" stroke="#666" width="150" height="65" rx="3" ry="3"
					  class="actor"></rect>
				<text x="275" y="372.5" style="text-anchor: middle;" dominant-baseline="central"
					  alignment-baseline="central" class="actor">
					<tspan x="275" dy="0">Our server</tspan>
				</text>
			</g>
		</svg>
	</div>
</div>
</div>


 We are excited to open source unk analytics .
          
          - Why we use multiple technologies to serve the app: 
          ### RoR
          
          ### Golang
          
          ### Node js
          
          ### Vuejs 
          
Me - Houssem Fathallah
